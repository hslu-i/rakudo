=begin pod
Übung Java Pow

Implementieren Sie eine Methode, welche für einen Array von Ganzzahlen diese Zahlen sowie die dazugehörigen Quadratzahlen ausgibt. Hinweis: Ihre Methode soll dazu in sinnvoller Weise die forEach-Methode vom Interface Iterable verwenden, verwandeln Sie den Array also zuerst in eine Collection. Die Signatur der zu implementierenden Methode sieht wie folgt aus:
public void printNumbersAndSquares(Integer[] numbers)
Der Aufruf dieser Methode mit dem Array {1, 2, 3, 5, 8} als Argument soll beispielsweise folgende Ausgabe produzieren:
1: 1
2: 4
3: 9
5: 25
8: 64

=end pod

for <1 2 3 5 8> {
  say  "$_ : " ~ ($_ ** 2);
}
