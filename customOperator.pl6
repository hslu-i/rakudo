
sub postfix:<!> ( Int $n ) {
    fail "parameter must be a number > 0" if $n < 0;
    [*] 2..$n
}

say 5!;

say 10000!;

say 5!.WHAT;

say 10000!.WHAT;
