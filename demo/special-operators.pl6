
## Substitution
say S/match/replacement/ given 'match string';

## Assessment
my $a min= 3;
$a min= 1;
$a min= 2;
say $a;


sub infix:<///> ($a, $b) { $a ~ "/" ~ $b };
my $b = "a";
$b ///= "b";
say $b;

## Hyper Operators
say (1, 2, 3, 4) »~» <a b>;   # (1a 2b 3a 4b)
say (1, 2, 3, 4) »*» (4, 3, 2, 1);   # (4 6 6 4)

# Unary Operation
my @array = 1,False,5;
say !« @array; # False True False

# recursive Hyper Operators
say -« [[1, 2], 3]; # [[-1 -2] -3]

## Reduce Operators
@array = 1,2,3;
say [+] @array;

say [~] @array;

## Cross Operators
1..3 X~ <a b>; # <1a, 1b, 2a, 2b, 3a, 3b>
