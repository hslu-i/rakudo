

my $promise1 = Promise.new;

my $promise2 = $promise1.then(
  -> $x { say $x.result; "Promise 2"}
);

$promise1.keep("Promise 1");

say $promise2.result;
