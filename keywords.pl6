# es gibt sehr viele keywords und spezielle syntax:

# unless
"hi there".say unless 0; # 0=false, 1=true, unless = if not

# smart matching ~~
(5 ~~ "5").say;
(<a b c> ~~ *,"c").say; # evaluiert zu true
(so (abc=>"test",c=>"string") ~~ /test/).say; # evaluiert zu true `so` für konvertierung zu bool

if Nil {say "Nil entspricht an false, semikolon ist bei der letzten Anweisung in einem Block optional"};

my $x = "--hoi---";
given $x {
  when Bool {say "uuh, a boolean"}
  when Int {say "a booring Int"}
  when /hoi/ {say "hello"}
  default {say "i dont know :'("}
} # given mach ein smartmatch


#`(
  finally multiline
  comments ... yay
)

sub func1($may?) {
  ("maybe " ~ $may ~ " is given").say
}
func1('b');

sub func2($default = "default") {
  ("at least $default is given").say
}
func2('b');

sub func3(*@dynamic) {
  for @dynamic { ("got an " ~ $_.WHAT.^name ~ " with value $_").say }
}

&func3(<a b c>, 1, pi);
