=begin pod
Ergänzen Sie die Funktion permutations mit einer anonymen Funktion (siehe Platzhalter
function-to-be-determined) so, dass aus einer beliebigen Liste eine Resultatliste aller
Permutationen dieser Liste erstellt wird.)

=end pod
my @l = <1 "hello" 2 3 pi "💪">;

say .join(" ") for @l.permutations
