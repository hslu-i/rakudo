use java::util::zip::CRC32:from<Java>;

my $crc = CRC32.new();

for 'Hello, Java'.encode('utf-8').list { # Appended `.list`
    $crc.'method/update/(I)V'($_);
}
say $crc.getValue();
