
if 'abcdef' ~~ / de / {
    say ~$/;            # de
    say $/.prematch;    # abc
    say $/.postmatch;   # f
    say $/.from;        # 3
    say $/.to;          # 5
};


'perl' ~~ /per./;       # matches the whole string (wildchard .)
'abc word' ~~ /\w/;     # matches a (\w = word)
'abc word' ~~ /\w+/;    # matches abc (+ = 1..∞)
'abc123word' ~~ /\W+/;  # uppercase = not

'123bb' ~~ /\d+/;       # matches 123

# \n = new line
# \s = space
# * 0..∞ match


'ABCpcpDEF' ~~ /[a..z]+/; # matches pcp
