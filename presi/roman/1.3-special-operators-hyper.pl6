## Hyper Operators

say (1, 2, 3, 4) »~» <a b>;        # (1a 2b 3a 4b)
say (1, 2, 3, 4) »*» (4, 3, 2, 1); # (4 6 6 4)


# Unary Operation
my @array = 1,False,5;
say !« @array; # False True False


# recursive Hyper Operators
say -« [[1, 2], 3]; # [[-1 -2] -3]
