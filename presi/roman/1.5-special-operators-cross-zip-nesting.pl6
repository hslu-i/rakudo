## Cross Operators
1..3 X~ <a b>; # <1a, 1b, 2a, 2b, 3a, 3b>

## Zip Operators
<a b c> Z~ 1, 2, 3; # [a1 b2 c3]

## Nesting
my @a = 1, 2, 3;
my @b = 5, 6, 7;
@a X[+=] @b;
say @a; # [19 20 21]
