# Promise erstellen
my $p1 = Promise.new;
say $p1.status;         # Planned
$p1.keep('result');
say $p1.status;         # Kept
say $p1.result;         # result


# Kürzer:
my $p2 = start {
    (1..Inf).grep(*.is-prime)[9999]
}
say $p2.result; # Blocking as in Java

###############################
# Chainable

my $p1 = Promise.new();
my $p2 = $p1.then(
    -> $v { say $v.result; "Second Result" }
);
$p1.keep("First Result");
say $p2.result;   # First Result \n Second Result

###############################

Await

my @result = await $p1, $p2;

my $promise = Promise.allof(
    Promise.in(2),
    Promise.in(3)
);
my $promise2 = Promise.anyof(
    Promise.in(3),
    Promise.in(8600)
);


###############################

# live Supply
my $supplier = Supplier.new;
my $supply   = $supplier.Supply;

$supply.tap( -> $v { say $v });

for 1 .. 10 {
    $supplier.emit($_);
}

# on-demand Supply
my $supply = supply {
    for 1 .. 10 {
        emit($_);
    }
}
$supply.tap( -> $v { say $v });

##############################
# Channels

my $channel = Channel.new;
$channel.send('Channel One');
say $channel.receive;  # 'Channel One'


# .list blockiert falls der Channel gerade leer ist. 
# und falls der Channel noch nicht geschlossen wurde.
for $channel.list -> $r {
    say $r;
}

# .poll blockiert nicht
my $is-closed = $c.closed;
loop {
    if $c.poll -> $item {
        say "$item received after {now - INIT now} seconds";
    }
    elsif $is-closed {
        last;
    }
    say 'Doing some unrelated things...';
    sleep .6;
}
