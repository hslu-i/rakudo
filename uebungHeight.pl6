=begin pod
Bei Skelettfunden schließt man aus der Länge der Knochen auf die Körpergröße; und zwar gilt (als
statistischer Mittelwert) in cm:
Körpergröße = 69.089 + 2.238 * Oberschenkelknochenlänge bei Männern
Körpergröße = 61.412 + 2.317 * Oberschenkelknochenlänge bei Frauen
Ab dem 30. Lebensjahr nimmt die Körpergröße um 0,06 cm pro Jahr ab.
a) Definieren Sie einen Datentyp man mit den Feldern Alter, Geschlecht und
Oberschenkelknochenlänge.
b) Erstellen Sie eine Funktion b-length, die aus einem Objekt vom Typ man die vermutete
Körpergrösse berechnet.
=end pod

class Human {

	has Int $.age;
	has Int $.length;
	has Bool $.male;

	method calculate-height returns Numeric {
		my $height;
		if ($.male) {
			$height = 69.089 + 2.238 * $.length;
		} else {
			$height = 61.412 + 2.317 * $.length;
		}

		if $.age >= 30 {
			$height = $height - ($.age-30) * 0.06;
		}
		$height;
	}
}

my $female = Human.new(age => 55, length => 50 , male => True);
say $female.calculate-height();

my $male = Human.new(age => 25, length => 49, male => False);
say $male.calculate-height();
