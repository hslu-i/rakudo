given $_ {
  when /test/ {
    say "got a test"
  }
  when 0..9 {
    say "got a single digit"
  }
  when <20 {
    say "number < 20"
  }

  default {
    say "nothing matched"
  }
}
