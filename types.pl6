# es gibt verschiedene Typen

my $a          = 10;                # typlos
my int $b      = 5;                 # statischer Typ
my $c          = 3;
my Str $string = "I am a string";

say $a, $b x 10, "+pcp\n";

# was heisst typlos?
my $x = 2;
my $y = "3.20000000000000000000001";

say "typlos: ", "\n x: ", $x, "\n y: ", $y, "\n summe: ", $x * $y, "\n";

say "ich kann auch pi: ", pi, " vom Typ: ", pi.WHAT;

# das Dollarsymbol nimmt beliebige Datentypen entgegen.
# Zusätzlich gibt es noch:
my @array = "a", "b", "c";
my %hash = a => 1, b => 2, c => "3", d => "d";
my &callable = { say "I am a function\n"; };

say @array, "\t", join ", ", @array;
say %hash;
callable();

my $irgendwas = @array;

say "irgendwas: ", $irgendwas;

# spezielles:
my @integers = 0 .. Inf;

say @integers[3];

for @integers -> $i {
    $i.say;
    last if $i > 10;
}
